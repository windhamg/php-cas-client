<?php
/*
 * Sample application showing usage of CASLogin class
 *
 */


require_once dirname(__FILE__) . '/CASLogin.php';

session_start();

if (!isset($_SESSION['_auth'])) {
    // user has not authenticated yet, so we call WebAuth
    $cas = new CASLogin;
    $res = $cas->checkAuth(array('banner' => 'My Cool App'));

    if (!$res) {  // If CASLogin::checkAuth returns false, determine why
        if ($cas->getLoginUrl()) {
            /*
             * authentication failed because user doesn't have a ticket yet.
             * redirect the user's browser to the WebAuth login URL
             */
            header("Location: " . $cas->getLoginUrl());
        } else {
            // some other error happened
            $errmsg = $cas->getAttribute("authenticationfailure");
            error_log("*** CASLogin::checkAuth failed: $errmsg");
            // any other error handling...
        }
        exit;
    } else {
        // set session variable to retain authentication status
        $_SESSION['_auth'] = true;

        // get any NetID attributes we need
        $_SESSION['netid'] = $cas->getAttribute("user");
        $_SESSION['dbkey'] = $cas->getAttribute("dbkey");
        $_SESSION['emplid'] = $cas->getAttribute("emplid");
    }
}
?>
<HTML>
<HEAD><TITLE>WebAuth test app</TITLE></HEAD>
<BODY>
<h3>PHP WebAuth Test App</h3>
<p>
<h3>User: <?= $_SESSION['netid'] ?></h3>
<h3>UA ID: <?= $_SESSION['dbkey'] ?></h3>
<?php if ($_SESSION['emplid']): ?>
<h3>EMPLID: <?= $_SESSION['emplid'] ?></h3>
<?php endif; ?>
</BODY>
</HTML>
