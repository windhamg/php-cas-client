<?php

// CAS URI definitions
define("CAS_BASE", "https://webauth.arizona.edu");
define("CAS_LOGIN_URI", "/webauth/login");
define("CAS_PROXY_URI", "/webauth/proxy");
define("CAS_VALIDATE_URI", "/webauth/serviceValidate");
define("CAS_PROXYVALIDATE_URI", "/webauth/proxyValidate");

// CAS XML namespace constants
define("CAS_AUTH_SUCCESS", "CAS:AUTHENTICATIONSUCCESS");
define("CAS_AUTH_FAILURE", "CAS:AUTHENTICATIONFAILURE");
define("CAS_PROXY_SUCCESS", "CAS:PROXYSUCCESS");
define("CAS_PROXY_FAILURE", "CAS:PROXYFAILURE");
define("CAS_PGTID", "CAS:PROXYGRANTINGTICKET");
define("CAS_PROXY_TICKET", "CAS:PROXYTICKET");
define("CAS_PROXY_LIST", "CAS:PROXIES");
define("CAS_PROXY", "CAS:PROXY");
define("CAS_AUTH_USERID", "CAS:USER");

class CASLogin {

  /*
   * instance variables needed for XML data processing
   */
  var $_Data = array();
  var $_CData = array();
   
  // counter for number of servers in proxy list
  var $_proxycnt;

  // authenticate status flag
  var $_auth;

  // CAS login URL
  var $_loginUrl = '';

  // XML parser handle
  var $_parser;

  /*
   * XML parser event handlers
   */

  //  Declare the function that runs each time an element starts.
  function _startHandler($Parser, $Elem, $Attr='') {
    // Start with empty CData array.
    $this->_CData = array();

    if ($Elem == CAS_AUTH_SUCCESS) {
      $this->_auth = true;
    }
    elseif ($Elem == CAS_AUTH_FAILURE) {
      $this->_auth = false;
    }
    elseif ($Elem == CAS_PROXY_LIST) {
      $this->_proxycnt = 0;
    }
    // Put each attribute into the Data array.
    while ( list($Key,$Val) = each($Attr) ) {
      $this->_Data["$Elem:$Key"] = trim($Val);
      //echo "$Elem:$Key = " . $Data["$Elem:$Key"] . "\n\n";
    }
  }  // End of function StartHandler

  // Declare the function that runs each time character data is encountered.
  function _characterHandler($Parser, $Line) {
    // Place lines into an array because elements can contain more than
    // one line of data.
    $this->_CData[] = $Line;
  }  //End of function CharacterHandler

  // Declare the function that runs each time an element ends.
  function _endHandler($Parser, $Elem) {
    // Mush all of the CData lines into a string and put it into the $Data array

    if ($Elem == CAS_PROXY) 
      $this->_Data[$Elem][$this->_proxycnt++] = trim( implode('', $this->_CData) );
    else 
      $this->_Data[$Elem] = trim( implode('', $this->_CData) );
    //echo "$Elem = " . $Data[$Elem] . "\n";
    $this->_CData = array();
  }

  // check if user is authenticated
  function checkAuth($params = array(), $logout_notify = false, $proxy = false, $proxyUrl = '', $proxyAuthList = array()) {

    // build CAS service name
    $host = $_SERVER["SERVER_NAME"];
    $port = $_SERVER["SERVER_PORT"];
    $uri = $_SERVER["PHP_SELF"];
    $get = $_SERVER["QUERY_STRING"];
    $proto = "http" . ((!empty($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != 'off') ? "s" : "") . "://";
    $service = $proto . $host . ":" . $port . $uri;

    if (!isset($_REQUEST["ticket"]) or empty($_REQUEST["ticket"])) {
      $this->_loginUrl = CAS_BASE . CAS_LOGIN_URI . "?service=" . urlencode($service . (!empty($get) ? "?$get" : ""));
      if (!empty($params)) {
        while (list($key, $val) = each($params)) {
          $this->_loginUrl .= "&$key=" . urlencode($val);
        }
      }
      return;
    }
    else {
      $serviceTicket = $_REQUEST["ticket"];

      // rebuild query string, minus ticket
      $get = "";
      if (count($_GET) > 1) {
        while (list($key, $val) = each($_GET)) {
          if ($key != "ticket") {
   	    if (!empty($get)) { $get .= "&"; }
            $get .= "$key=$val";
	  }
        }
      }
      $url =  CAS_BASE . ($proxy ? CAS_PROXYVALIDATE_URI : CAS_VALIDATE_URI) . "?service=" . urlencode($service . (!empty($get) ? "?$get" : "")) . "&ticket=" . $serviceTicket . ($logout_notify ? "&logout_notify=1" : "") . ($proxy ? "&pgtUrl=" . urlencode($proxyUrl) : "");
      $this->_Data = @file_get_contents($url);

      $authresult = htmlspecialchars($this->_Data);

      $contents = $this->_Data;

      // Initialize the parser.
      $this->_parser = xml_parser_create('ISO-8859-1');
      xml_set_object($this->_parser, $this);
      xml_set_element_handler($this->_parser, '_startHandler', '_endHandler');
      xml_set_character_data_handler($this->_parser, '_characterHandler');

      // parse XML data
      $this->_Data = array();
      $this->_auth = false;
      if (!xml_parse($this->_parser, $contents) ) {
        error_log("Error parsing XML data: " . xml_error_string(xml_get_error_code($this->_parser)));
      }
    }

    // check results
    if ($this->_auth) {
      // Check proxy chain to see if proxy(s) are allowed, if present
      if ($this->_proxycnt) {
        for ($i=0; $i<(count($proxyAuthList) >= $this->_proxycnt ? $this->_proxycnt : count($proxyAuthList)); $i++) {
           if ($this->_Data[CAS_PROXY][$i] != $proxyAuthList[$i]) {
             $this->_auth = false;
             break;
           }
        }
      }
    }
    return $this->_auth;
  }

  // return the URL to redirect browser to for inital CAS authentication
  function getLoginUrl() {
    return $this->_loginUrl;
  }

  // return the requested attribute from the XML CAS response
  function getAttribute($attr = '') {
    return $this->_Data['CAS:' . strtoupper($attr)];
  }

  // get a PGT corresponding to the pgtIOU provided
  function GetProxyTicket($pgtid, $service) {
    global $auth, $Data, $authresult;
  
    $url = CAS_BASE . CAS_PROXY_URI . "?targetService=" . urlencode($service) . "&pgt=" . $this->_FetchPGT($pgtid);
    $this->_Data = @file_get_contents($url);
  
    $authresult = htmlspecialchars($this->_Data);
  
    $contents = $this->_Data;
  
    // Initialize the parser.
    $this->_parser = xml_parser_create('ISO-8859-1');
    xml_set_object($this->_parser, $this);
    xml_set_element_handler($this->_parser, '_startHandler', '_endHandler');
  
    // parse XML data
    $this->_Data = array();
    $this->_auth = false;

    if (!xml_parse($this->_parser, $contents) ) {
      error_log("Error parsing XML data: " . xml_error_string(xml_get_error_code($this->_parser)));
      return FALSE;
    }
    if (isset($this->_Data[PROXY_TICKET])) {
      return $this->_Data[PROXY_TICKET];
    } else {
      return FALSE;
    }
  }

  /*
   * Sample function to retrieve a PGT by ID from shared memory
   * Other places to store PGT/PGTIOU pairs would be a database, file, etc
   */
  function _FetchPGT($pgtid) {
    $shmid = shmop_open(0x0fff, "a", 0, 0);
    $pgtid_tbl = unserialize(shmop_read($shmid, 0, shmop_size($shmid)));
    if (isset($pgtid_tbl[$pgtid])) {
      return $pgtid_tbl[$pgtid];
    } else {
      return -1;
    }
  }
}
